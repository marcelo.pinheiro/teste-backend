Rails.application.routes.draw do
  resources :redes do
    post :auth
    resources :usuarios
    resources :visitantes do
      post :visitante_foto
      post :visitante_registro
    end
  end

  post 'auth/login', to: 'authentication#authenticate'
  get 'auth/logout', to: 'authentication#logout'
  
end
